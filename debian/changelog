gnome-taquin (3.38.1-2) unstable; urgency=medium

  * Cherry-pick 2 patches to fix build with latest vala (Closes: #997196)

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 23 Oct 2021 16:10:49 -0400

gnome-taquin (3.38.1-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@debian.org>  Mon, 05 Oct 2020 17:56:59 +0200

gnome-taquin (3.38.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in:
    - updated gtk requirement

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 15 Sep 2020 21:05:18 +0200

gnome-taquin (3.36.2-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper-compat to 13
  * Bump Standards-Version to 4.5.0

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 05 May 2020 20:39:18 -0400

gnome-taquin (3.36.0-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 13 Mar 2020 16:01:38 +0100

gnome-taquin (3.35.91-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in:
    - updated the glib requirement to 2.42

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 26 Feb 2020 10:19:11 +0100

gnome-taquin (3.34.1-1) unstable; urgency=medium

  * New upstream translations update

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 10 Oct 2019 06:21:32 -0400

gnome-taquin (3.34.0-1) unstable; urgency=medium

  * New upstream release
  * Build-Depend on libgsound-dev instead of libcanberra-gtk3-dev
  * Bump minimum GTK3 to 3.22.23
  * Bump Standards-Version to 4.4.0
  * Upload to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 27 Sep 2019 19:16:22 -0400

gnome-taquin (3.32.0-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 12 Mar 2019 20:07:37 -0400

gnome-taquin (3.31.90-1) experimental; urgency=medium

  * New upstream development release
  * Build with meson
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Build-Depend on dh-sequence-gnome
  * Build-Depend on libxml2-utils for xmllint

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 08 Feb 2019 12:12:38 -0500

gnome-taquin (3.30.0-2) unstable; urgency=medium

  * Add -Wl,-01 to LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Dec 2018 16:43:14 -0500

gnome-taquin (3.30.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 08 Oct 2018 00:01:19 -0400

gnome-taquin (3.28.0-2) unstable; urgency=medium

  * Add X-Ubuntu-Use-Langpack to opt in to Ubuntu language pack handling
    (LP: #1779574)
  * Bump Standards-Version to 4.2.1

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 05 Sep 2018 14:26:02 -0400

gnome-taquin (3.28.0-1) unstable; urgency=medium

  * New upstream release
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Add src/*.vala.stamp to Files-Excluded to force build from vala sources
  * Bump debhelper compat to 11

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 15 Mar 2018 17:44:11 -0400

gnome-taquin (3.26.1-2) unstable; urgency=medium

  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 15 Dec 2017 17:40:12 -0500

gnome-taquin (3.26.1-1) unstable; urgency=medium

  * New upstream translations release
  * Bump Standards-Version to 4.1.1

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 01 Oct 2017 15:33:12 -0400

gnome-taquin (3.22.0-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Tue, 20 Sep 2016 21:32:38 +0200

gnome-taquin (3.21.92-1) unstable; urgency=medium

  * New upstream development release.

 -- Michael Biebl <biebl@debian.org>  Tue, 13 Sep 2016 09:54:37 +0200

gnome-taquin (3.21.91-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release
  * debian/control.in:
    - Update Vcs fields
  * Bump debhelper compat to 10
  * Delete vala stamps to rebuild generated source files
  * debian/copyright: Include info about sound effects
  * Install COPYING.sounds and COPYING.themes since they are
    referenced in d/copyright and the app's About screen

 -- Michael Biebl <biebl@debian.org>  Tue, 30 Aug 2016 16:16:24 +0200

gnome-taquin (3.20.2-2) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Install to /usr/games/ as per FHS and Debian policy
  * Add debian/docs to install NEWS
  * Enable all hardening flags
  * Run autoreconf

 -- Michael Biebl <biebl@debian.org>  Thu, 26 May 2016 06:59:15 +0200

gnome-taquin (3.20.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8.

 -- Michael Biebl <biebl@debian.org>  Wed, 18 May 2016 17:28:36 +0200

gnome-taquin (3.20.1-1) unstable; urgency=medium

  [ Michael Biebl ]
  * Drop uploaders.mk from debian/rules as this breaks the clean target with
    dh. Instead use the gnome dh addon which updates debian/control via
    dh_gnome_clean.

  [ Andreas Henriksson ]
  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 18 Apr 2016 18:20:30 +0200

gnome-taquin (3.19.91-1) unstable; urgency=low

  * Initial release

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 01 Mar 2016 09:45:28 +0100
