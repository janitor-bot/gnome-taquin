# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Gnome Nepali Translation Project\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"taquin&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2017-03-22 07:27+0000\n"
"PO-Revision-Date: 2017-08-18 10:41+0545\n"
"Language-Team: Nepali Translation Team <chautari@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.3\n"
"Last-Translator: Pawan Chitrakar <chautari@gmail.com>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: ne\n"

#: ../data/taquin-menus.ui.h:1
msgid "_Sound"
msgstr "ध्वनि"

#: ../data/taquin-menus.ui.h:2
msgid "_Help"
msgstr "मद्दत"

#: ../data/taquin-menus.ui.h:3
msgid "_About"
msgstr "बारेमा"

#: ../data/taquin-menus.ui.h:4
msgid "_Quit"
msgstr "बन्द गर्नुहोस्"

#: ../data/taquin-screens.ui.h:1
msgid "3 × 3"
msgstr "३ × ३"

#: ../data/taquin-screens.ui.h:2
msgid "4 × 4"
msgstr "४ × ४"

#: ../data/taquin-screens.ui.h:3
msgid "5 × 5"
msgstr "५ × ५"

#: ../data/taquin-screens.ui.h:4
msgid "Cats"
msgstr "बिरालो"

#: ../data/taquin-screens.ui.h:5
msgid "Numbers"
msgstr "सङ्ख्याहरू"

#: ../data/taquin-screens.ui.h:6 ../data/org.gnome.taquin.desktop.in.h:5
msgid "15-Puzzle"
msgstr "१५-पजल खेल"

#: ../data/taquin-screens.ui.h:7 ../data/org.gnome.taquin.desktop.in.h:6
msgid "16-Puzzle"
msgstr "१६-पजल खेल"

#: ../data/taquin.ui.h:1
msgid "Go back to the current game"
msgstr "हालको खेलमा फर्कनुहोस्"

#: ../data/taquin.ui.h:2
msgid "_Start Over"
msgstr "सुरुआत गर्नुहोस्"

#: ../data/taquin.ui.h:3
msgid "Configure a new game"
msgstr "नयाँ खेल कन्फिगर गर्नुहोस्"

#: ../data/taquin.ui.h:4
msgid "Start a new game"
msgstr "नयाँ खेल सुरु गर्नुहोस्"

#: ../data/org.gnome.taquin.appdata.xml.in.h:1
msgid "GNOME Taquin"
msgstr "जिनोम टाक्विन"

#: ../data/org.gnome.taquin.appdata.xml.in.h:2
#: ../data/org.gnome.taquin.desktop.in.h:3
msgid "Slide tiles to their correct places"
msgstr "सहि स्थानमा टायलहरू स्लाइड गर्नुहोस्"

#: ../data/org.gnome.taquin.appdata.xml.in.h:3
msgid ""
"Taquin is a computer version of the 15-puzzle and other sliding puzzles."
msgstr "टाक्विन १५-पजल र अन्य स्लाइडिङ्ग खेल को  कम्प्युटर संस्करण हो।"

#: ../data/org.gnome.taquin.appdata.xml.in.h:4
msgid ""
"The object of Taquin is to move tiles so that they reach their places, "
"either indicated with numbers, or with parts of a great image."
msgstr ""
"टाक्विनको उदेश्य टायल सहि स्थानमा सार्नु हो , टायल संख्या वा छवि को भागहरू मा सङ्केत हुन्छ"

#: ../data/org.gnome.taquin.appdata.xml.in.h:5
msgid "A GNOME taquin game preview"
msgstr "जिनोम टाक्विन खेल पुर्वावलोकन"

#: ../data/org.gnome.taquin.appdata.xml.in.h:6
msgid "The GNOME Project"
msgstr "जिनोम परियोजना"

#: ../data/org.gnome.taquin.desktop.in.h:1 ../src/taquin-main.vala:76
#: ../src/taquin-main.vala:133 ../src/taquin-main.vala:237
msgid "Taquin"
msgstr "टाक्विन"

#: ../data/org.gnome.taquin.desktop.in.h:2
msgid "15-puzzle"
msgstr "१५-पजल खेल"

#: ../data/org.gnome.taquin.desktop.in.h:4
msgid "puzzle;"
msgstr "पजल;"

#: ../data/org.gnome.taquin.gschema.xml.h:1
msgid "Number of tiles on each edge."
msgstr "प्रत्येक किनारमा टायलहरूको सङ्ख्या"

#: ../data/org.gnome.taquin.gschema.xml.h:2
msgid ""
"The game offers to play with a board size from 3 to 5. The setting could be "
"set from 2 (good for testing) to 9, limited by the way files are selected."
msgstr ""
"खेलले ३ देखि ५ सम्म बोर्ड साइज खेल्न प्रस्ताव गर्दछ। सेटिङ २ (परीक्षणको लागि राम्रो) देखि "
"९सम्म हुनसक्छ, फाइलहरू चयन गरिएका फाइलहरू द्वारा सीमित हुन्छ"

#: ../data/org.gnome.taquin.gschema.xml.h:3
msgid "Name of the theme folder."
msgstr "विषयवस्तु फोल्डरको नाम"

#: ../data/org.gnome.taquin.gschema.xml.h:4
msgid "Taquin has two default themes: 'cats' and 'numbers'."
msgstr "ताइक्न दुई डिफल्ट विषयवस्तुहरू छन्: 'बिरालो' र 'सङ्ख्या'।"

#: ../data/org.gnome.taquin.gschema.xml.h:5
msgid "Sound"
msgstr "ध्वनि"

#: ../data/org.gnome.taquin.gschema.xml.h:6
msgid "Whether or not to play event sounds."
msgstr "घटना ध्वनि प्ले गर्ने या नगर्ने ।"

#: ../data/org.gnome.taquin.gschema.xml.h:7
msgid "Width of the window in pixels."
msgstr "मुख्य सञ्झ्यालको चौडाइ पिक्सेलमा"

#: ../data/org.gnome.taquin.gschema.xml.h:8
msgid "Height of the window in pixels."
msgstr "मुख्य सञ्झ्यालको उचाइ पिक्सेलमा"

#: ../data/org.gnome.taquin.gschema.xml.h:9
msgid "true if the window is maximized."
msgstr "सत्य यदि सञ्झ्याल अधिकतम छ।"

#: ../src/game-window.vala:128
msgid "_Start Game"
msgstr "खेल सुरु गर्नुहोस्"

#: ../src/game-window.vala:153
msgid "Undo your most recent move"
msgstr "हालको चाल पूर्ववत गर्नुहोस्"

#: ../src/taquin-main.vala:45
msgid "Play the classical 1880s’ 15-puzzle"
msgstr "क्लासिकल १८८० को १५-पजल खेल्नुहोस्"

#: ../src/taquin-main.vala:46
msgid "Try this fun alternative 16-puzzle"
msgstr "यो रमाइलो वैकल्पिक १६-पजल प्रयास गर्नुहोस्"

#: ../src/taquin-main.vala:47
msgid "Sets the puzzle edges’ size (3-5, 2-9 for debug)"
msgstr "पजल किनाराहरूको आकार सेट गर्दछ (३-५, डिबगको लागि २-९)"

#: ../src/taquin-main.vala:48
msgid "Turn off the sound"
msgstr "ध्वनि बन्द गर्नुहोस्"

#: ../src/taquin-main.vala:49
msgid "Turn on the sound"
msgstr "ध्वनि खोल्नुहोस्"

#: ../src/taquin-main.vala:50
msgid "Print release version and exit"
msgstr "मुद्रण संस्करण विवरण अनि बाहिरनुहोस्"

#: ../src/taquin-main.vala:233
msgid "(see COPYING.themes for informations)"
msgstr "(सुचनाको लागि COPYING.themes हेर्नुहोस्)"

#: ../src/taquin-main.vala:241
msgid "A classic 15-puzzle game"
msgstr "एक क्लासिक १५-पजल खेल"

#: ../src/taquin-main.vala:245
msgid "translator-credits"
msgstr ""
"अनुवादक श्रेय\n"
"\n"
"Launchpad Contributions:\n"
"  Nabin Gautam https://launchpad.net/~nabin"

#: ../src/taquin-main.vala:286
msgid "You can’t move this tile!"
msgstr "यो टायल सार्न सकिदैन"

#: ../src/taquin-main.vala:292
msgid "Bravo! You finished the game!"
msgstr "वाह ! तपाईँले खेल समाप्त गर्नुभयो !"

#: ../src/taquin-main.vala:310
#, c-format
msgid "Size: %d × %d ▾"
msgstr "Size: %d × %d ▾"

#: ../src/taquin-main.vala:324
msgid "Theme: Cats ▾"
msgstr "विषयवस्तु:बिरालो▾"

#: ../src/taquin-main.vala:325
msgid "Theme: Numbers ▾"
msgstr "विषयवस्तु:सङ्ख्या ▾"
